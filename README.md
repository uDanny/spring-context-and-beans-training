#### **Spring Context and Beans lifecycle Practice**
Training with Spring beans, defining the order of invocations of beans shortcuts, the right way of using **BeanPostProcessor**, **ContextRefreshedEvent**, **BeanFactoryPostProcessor**, **@PostConstruct**, **custom Annotations** and the reflection tools for retrieving Beans info like (**Bean Definition**) as well as the moment instances are wrapped into proxies.
Also created an **mBean** controller, for enabling profiling feature.
Creates Custom Context for property file.
Defined a **BeanFactoryPostProcessor** which works before beans and instances created, but after bean definitions creations.
Also the injection **Prototype** beans into a **Singleton** problem is resolveD in 6 different solutions.
Also developed a new scope for the bean prototype strategy.

!!! Exercises are not production ready, and not all of them are written in a clean code form, or offers ready solutions, most of them are for demonstration of Spring components possibilities !!!

Order of method invocations:
 - BeanFactoryPostProcessor 
 - constructor
 - before initialization method from BeanPostProcessor
 - @PostConstruct annotated init method
 - after initialization method from BeanPostProcessor
 - ContextRefreshedEvent Listener

Hints:
 - At the commit: https://gitlab.com/uDanny/spring-context-and-beans-training/tree/add-profiler-bean-post-processor :
  **Added the Profiler and BeanPostProcessor usage**
 - At the commit: https://gitlab.com/uDanny/spring-context-and-beans-training/tree/added-the-context-refreshed-event :
  **Added the ContextRefreshedEvent**
 - At the commit: https://gitlab.com/uDanny/spring-context-and-beans-training/tree/added-bean-factory-post-processor :
  **Added the BeanFactoryPostProcessor and change the Implementation of Interface**
 - At the commit: https://gitlab.com/uDanny/spring-context-and-beans-training/tree/added-properties-file-spring-context :
  **Added the new created Context from properties file**
 - At the commit: https://gitlab.com/uDanny/spring-context-and-beans-training/tree/inject-prototype-into-singleton :
  **Added examples of resolving the problem of injecting Prototype into Singleton**
 - At the commit: https://gitlab.com/uDanny/spring-context-and-beans-training/tree/custom-bean-scope :
  **Created a custom scope for the beans lifeCycle**

