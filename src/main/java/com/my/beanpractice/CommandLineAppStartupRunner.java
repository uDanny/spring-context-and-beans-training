package com.my.beanpractice;

import com.my.beanpractice.postProcessor.RunPeriodical;
import com.my.beanpractice.singletonVsPrototype.PrototypeInjectProblemRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class CommandLineAppStartupRunner implements CommandLineRunner {
    @Autowired
    private ApplicationContext context;
    @Autowired
    private PrototypeInjectProblemRunner prototypeInjectProblemRunner;
    @Autowired
    RunPeriodical runPeriodical;

    @Override
    public void run(String... args) throws Exception {
//removing invocation, it will be triggered by on Context Refresh
//        while (true) {
//            Thread.sleep(100);
        //context.getBean(TerminatorQuoter.class).sayQuote();
        //context.getBean(Quoter.class).getClass() is $Proxy
        //return Proxy instead of instance, that's why lookup should be made by interfaces

//            context.getBean(Quoter.class).sayQuote();

//        }

//        context.getBean(Quoter.class).sayQuote();


//        prototypeInjectProblemRunner.start();


        runPeriodical.run();
    }


}
