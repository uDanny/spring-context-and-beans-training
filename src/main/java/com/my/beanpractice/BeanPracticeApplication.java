package com.my.beanpractice;

import com.my.beanpractice.singletonVsPrototype.PrototypeBean;
import com.my.beanpractice.singletonVsPrototype.SingletonBean;
import com.my.beanpractice.postProcessor.DeprecationHandlerBeanFactoryPostProcessor;
import com.my.beanpractice.postProcessor.InjectRandomIntAnnotationBeanPostProcessor;
import com.my.beanpractice.postProcessor.PostProxyInvokerContextListener;
import com.my.beanpractice.postProcessor.ProfilingAnnotationBeanPostProcessor;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanRegistrationException;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;

@SpringBootApplication
public class BeanPracticeApplication {
    /**
     * order of method invocations:
     * BeanFactoryPostProcessor
     * constructor
     * before initialization method from BeanPostProcessor
     *
     * @PostConstruct annotated init method
     * after initialization method from BeanPostProcessor
     * ContextRefreshedEvent Listener
     */


    public static void main(String[] args) {
        SpringApplication.run(BeanPracticeApplication.class, args);
    }

    @Bean
    InjectRandomIntAnnotationBeanPostProcessor createPostProcessor() {
        return new InjectRandomIntAnnotationBeanPostProcessor();
    }

    @Bean
    ProfilingAnnotationBeanPostProcessor createController() throws MalformedObjectNameException, NotCompliantMBeanException, InstanceAlreadyExistsException, MBeanRegistrationException {
        return new ProfilingAnnotationBeanPostProcessor();
    }

    @Bean
    DeprecationHandlerBeanFactoryPostProcessor createBeanFactory() {
        return new DeprecationHandlerBeanFactoryPostProcessor();
    }

    @Bean
    PostProxyInvokerContextListener createContextListener() {
        return new PostProxyInvokerContextListener();
    }

    //removed @Bean, and added @Component and @Value, because on creation by @Bean, BeanDefinition.getBeanClassName == null, Spring doesn't save the link to original class
//    @Bean
//    TerminatorQuoter createQuoter() {
//        TerminatorQuoter terminatorQuoter = new TerminatorQuoter();
//        terminatorQuoter.setMessage("I'll be back");
//        return terminatorQuoter;
//    }

}

