package com.my.beanpractice.mBean;

import lombok.Getter;
import lombok.Setter;

public class ProfileController implements ProfileControllerMBean {
    @Getter
    @Setter
    private boolean enabled;
}
