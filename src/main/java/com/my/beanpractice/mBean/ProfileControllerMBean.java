package com.my.beanpractice.mBean;

public interface ProfileControllerMBean {
    boolean isEnabled();

    void setEnabled(boolean enabled);
}
