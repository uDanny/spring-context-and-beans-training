package com.my.beanpractice.postProcessor;

import com.my.beanpractice.annotation.InjectRandom;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.Random;

public class InjectRandomIntAnnotationBeanPostProcessor implements BeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        Field[] fields = bean.getClass().getDeclaredFields();
        for (Field field : fields) {
            InjectRandom annotation = field.getAnnotation(InjectRandom.class);
            if (annotation != null) {
                int max = annotation.max();
                int min = annotation.min();
                Random r = new Random();
                int i = min + r.nextInt(max - min);
                field.setAccessible(true);
//                field.set(); // it requires catching checked exceptions
                ReflectionUtils.setField(field, bean, i);

            }
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }
}
