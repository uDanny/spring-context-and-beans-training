package com.my.beanpractice.postProcessor;

import com.my.beanpractice.annotation.PostProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import java.lang.reflect.Method;

public class PostProxyInvokerContextListener implements ApplicationListener<ContextRefreshedEvent> { //ContextRefreshedEvent is  triggered after all proxies are created
    @Autowired
    private ConfigurableListableBeanFactory beanFactory;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        ApplicationContext context = contextRefreshedEvent.getApplicationContext();
        String[] beanDefinitionNames = context.getBeanDefinitionNames();
        for (String beanDefinitionName : beanDefinitionNames) {
            BeanDefinition beanDefinition = beanFactory.getBeanDefinition(beanDefinitionName); //bean definition is better for searching info that needed (also better inc ase of lazy beans)
            String originalClassName = beanDefinition.getBeanClassName(); //we can retrieve original class name, not the proxy is  already created
            try {
                Class<?> originalClass = Class.forName(originalClassName);
                if (originalClass != null) {
                    Method[] methods = originalClass.getMethods();
                    for (Method method : methods) {
                        if (method.isAnnotationPresent(PostProxy.class)) {
//                            method.invoke() // won't  work through DynamicProxy (only through CGLib)
                            Object bean = context.getBean(beanDefinitionName); //now we retrieved the bean == Proxy's class
                            Method currentMethod = bean.getClass().getMethod(method.getName(), method.getParameterTypes());
                            currentMethod.invoke(bean);
                        }
                    }
                }
            } catch (Exception e) {
            }
        }
    }
}

