package com.my.beanpractice.postProcessor;

import com.my.beanpractice.model.PeriodicalBean;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Service;

import static org.springframework.util.Assert.isTrue;

@Service
public class RunPeriodical {

    @Lookup
    PeriodicalBean getPeriodicalBean() {
        return null;
    }

    public void run() throws InterruptedException {
        PeriodicalBean periodicalBean = getPeriodicalBean();
        Thread.sleep(7000);
        PeriodicalBean periodicalBean2 = getPeriodicalBean();

        isTrue(periodicalBean != periodicalBean2, "Instances aren't different");
    }
}
