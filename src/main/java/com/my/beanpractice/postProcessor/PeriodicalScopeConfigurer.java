package com.my.beanpractice.postProcessor;


import javafx.util.Pair;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.config.Scope;

import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;

import static java.time.LocalTime.now;

public class PeriodicalScopeConfigurer implements Scope {
    Map<String, Pair<LocalTime, Object>> map = new HashMap<>();

    @Override
    public Object get(String key, ObjectFactory<?> objectFactory) {
        if (map.containsKey(key)) {
            Pair<LocalTime, Object> value = map.get(key);
            if (value.getKey().plusSeconds(5).isBefore(now())) {
                map.put(key, new Pair<>(now(), objectFactory.getObject()));
            }
        } else {
            map.put(key, new Pair<>(now(), objectFactory.getObject()));

        }
        return map.get(key).getValue();
    }

    @Override
    public Object remove(String s) {
        return null;
    }

    @Override
    public void registerDestructionCallback(String s, Runnable runnable) {

    }

    @Override
    public Object resolveContextualObject(String s) {
        return null;
    }

    @Override
    public String getConversationId() {
        return null;
    }
}
