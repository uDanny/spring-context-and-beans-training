package com.my.beanpractice.singletonVsPrototype;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.time.LocalTime;

public class SingletonBean {

    @Autowired
    @Qualifier("prototypeBean")
    private PrototypeBean prototypeBean;

    public SingletonBean() {
        System.out.println("Singleton instance created");
    }

    public PrototypeBean getPrototypeBean() {
        System.out.println(LocalTime.now());
        return prototypeBean;
    }
}
