package com.my.beanpractice.singletonVsPrototype;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

import java.util.function.Function;
import java.util.function.Supplier;

@Configuration
public class BeansConfig {

    @Bean
    public SingletonBean singletonBean() {
        return new SingletonBean();
    }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public PrototypeBean prototypeBean() {
        return new PrototypeBean();
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public PrototypeBean prototypeBeanSolution2() {
        return new PrototypeBean();
    }

    @Bean
    public SingletonBeanForSolution3 singletonBeanSolution3() {
        return new SingletonBeanForSolution3() {
            @Override
            protected PrototypeBean getPrototypeBean() {
                return prototypeBean(); //call the prototype bean
            }
        };
    }
    @Bean
    public Supplier<PrototypeBean> beanFactory() {
        return () -> prototypeBean();
    }
}
