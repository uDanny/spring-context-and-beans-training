package com.my.beanpractice.singletonVsPrototype;

import lombok.Data;
import org.springframework.aop.framework.Advised;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.function.Supplier;

import static org.springframework.util.Assert.isTrue;

@Service
public class PrototypeInjectProblemRunner {

    @Autowired
    private ApplicationContext context;

    public void start() throws Exception {

        System.out.println("PrototypeInjectProblemRunner");

        SingletonBean firstSingleton = context.getBean(SingletonBean.class);
        PrototypeBean firstPrototype = firstSingleton.getPrototypeBean();

        // get singleton bean instance one more time
        SingletonBean secondSingleton = context.getBean(SingletonBean.class);
        PrototypeBean secondPrototype = secondSingleton.getPrototypeBean();

        isTrue(firstPrototype.equals(secondPrototype), "The same instance should be returned");

        solution1();
        solution2();
        solution3();
        solution4();
        solution5();
        solution6();
    }

    private void solution1() {
        //it could be resolved by the injecting of the context into singleton, and return the prototype from context
        //but it's a bad design, because of coupling with framework

        SingletonBeanForSolution1 singletonBean = context.getBean(SingletonBeanForSolution1.class);
        PrototypeBean firstPrototype = singletonBean.getPrototypeBean();

        SingletonBeanForSolution1 singletonBean2 = context.getBean(SingletonBeanForSolution1.class);
        PrototypeBean secondPrototype = singletonBean2.getPrototypeBean();

        isTrue(firstPrototype != secondPrototype, "Instances are different");


    }

    private void solution2() throws Exception {
        //it could be resolved by changing the proxyMode of the bean created
        //the drawback is that even from inside the singleton, every call to prototype instance, will trigger a new object
        //in this example we got 6 invocations

        SingletonBeanForSolution2 firstSingleton = context.getBean(SingletonBeanForSolution2.class);
        PrototypeBean firstPrototype = firstSingleton.getPrototypeBean();

        SingletonBeanForSolution2 secondSingleton = context.getBean(SingletonBeanForSolution2.class);
        PrototypeBean secondPrototype = secondSingleton.getPrototypeBean();

        isTrue(firstPrototype == secondPrototype, "Instances are different"); //because points to the proxies equals
        //but:
        isTrue(((Advised) firstPrototype).getTargetSource().getTarget() != ((Advised) secondPrototype).getTargetSource().getTarget(), "Instances are equals");


    }

    private void solution3() {
        //the best solution is to make singleton getter of prototype abstract,
        //and implement that inside of the Spring Config, by calling prototype bean

        SingletonBeanForSolution3 firstSingleton = context.getBean(SingletonBeanForSolution3.class);
        PrototypeBean firstPrototype = firstSingleton.getPrototypeBean();

        SingletonBeanForSolution3 secondSingleton = context.getBean(SingletonBeanForSolution3.class);
        PrototypeBean secondPrototype = secondSingleton.getPrototypeBean();

        isTrue(firstPrototype != secondPrototype, "Instances are different");

    }

    private void solution4() {
        //@Lookup is also a good solution, Spring overrides our implementation

        SingletonBeanForSolution4 firstSingleton = context.getBean(SingletonBeanForSolution4.class);
        PrototypeBean firstPrototype = firstSingleton.getPrototypeBean();

        SingletonBeanForSolution4 secondSingleton = context.getBean(SingletonBeanForSolution4.class);
        PrototypeBean secondPrototype = secondSingleton.getPrototypeBean();

        isTrue(firstPrototype != secondPrototype, "Instances are different");

    }

    private void solution5() throws Exception {
        //here is used object ObjectFactory and geObject for obtaining new obj
        //there also exists an alternative for that by using  javax.inject.Provider, which is used in the same manner

        SingletonBeanForSolution5 firstSingleton = context.getBean(SingletonBeanForSolution5.class);
        PrototypeBean firstPrototype = firstSingleton.getPrototypeBean();

        SingletonBeanForSolution5 secondSingleton = context.getBean(SingletonBeanForSolution5.class);
        PrototypeBean secondPrototype = secondSingleton.getPrototypeBean();

        isTrue(firstPrototype == secondPrototype, "Instances are different"); //because points to the proxies equals
        //but:
        isTrue(((Advised) firstPrototype).getTargetSource().getTarget() != ((Advised) secondPrototype).getTargetSource().getTarget(), "Instances are equals");


    }

    private void solution6() {
        //Function Bean is also an elegant solution of resolving the injecting problem

        SingletonBeanForSolution6 firstSingleton = context.getBean(SingletonBeanForSolution6.class);
        PrototypeBean firstPrototype = firstSingleton.getPrototypeBean();

        SingletonBeanForSolution6 secondSingleton = context.getBean(SingletonBeanForSolution6.class);
        PrototypeBean secondPrototype = secondSingleton.getPrototypeBean();

        isTrue(firstPrototype != secondPrototype, "Instances are different");

    }

}

@Service
class SingletonBeanForSolution1 {

    @Autowired
    private ApplicationContext context;

    public SingletonBeanForSolution1() {
        System.out.println("Singleton instance created");
    }

    PrototypeBean getPrototypeBean() {
        return context.getBean("prototypeBean", PrototypeBean.class);
    }
}

@Service
@Data
class SingletonBeanForSolution2 {
    @Autowired
    @Qualifier(value = "prototypeBeanSolution2")
    PrototypeBean prototypeBean;

    PrototypeBean getPrototypeBean() {
        return prototypeBean;
    }
}


abstract class SingletonBeanForSolution3 {
    protected abstract PrototypeBean getPrototypeBean();
}

@Component
class SingletonBeanForSolution4 {

    @Lookup("prototypeBean")
    public PrototypeBean getPrototypeBean() {
        return null;
    }
}

@Component
class SingletonBeanForSolution5 {

    @Autowired
    @Qualifier(value = "prototypeBeanSolution2")
    private ObjectFactory<PrototypeBean> myPrototypeBeanProvider;

    public PrototypeBean getPrototypeBean() {
        return myPrototypeBeanProvider.getObject();
    }
}

@Component
class SingletonBeanForSolution6 {

    @Autowired
    private Supplier<PrototypeBean> beanFactory;

    public PrototypeBean getPrototypeBean() {
        return beanFactory.get();
    }

}
