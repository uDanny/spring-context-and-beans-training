package com.my.beanpractice.model;

import com.my.beanpractice.annotation.DeprecatedClass;
import com.my.beanpractice.annotation.InjectRandom;
import com.my.beanpractice.annotation.PostProxy;
import com.my.beanpractice.annotation.Profiling;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Profiling
@Component
@DeprecatedClass(newImpl = T100.class)
public class TerminatorQuoter implements Quoter {
    @InjectRandom(min = 2, max = 7)
    @Setter //setter for properties file value injection
    private int random;
    @Setter
    @Value("I'll be back")
    private String message;

    @PostConstruct // it works before proxies wrapping
    public void init() {
        System.out.println("Phase 2");
        System.out.println(random); //will show always random value, it's after BPP adding

    }

    public TerminatorQuoter() {
        System.out.println("Phase 1");
        System.out.println(random); //will show always 0 value, it's before BPP adding
    }

    @Override
    @PostProxy
    public void sayQuote() {
        for (int i = 0; i < random; i++) {
            System.out.println("message: " + message);
        }
    }
}
