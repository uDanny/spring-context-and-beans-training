package com.my.beanpractice.model;

public class T100 extends TerminatorQuoter implements Quoter { //double implements Quoter to avoid recursive search for getInterfaces of parents of T1000 (it's returning just the next upper level)
    @Override
    public void sayQuote() {
        System.out.print("I am liquid");
    }
}
